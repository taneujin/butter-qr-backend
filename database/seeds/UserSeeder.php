<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::insert([
            'merchant_id' => '1',
            'username' => 'butterQrDev',
            'password' => bcrypt('password123'),
            'role_id' => '1',
            'created_by' => 'seeder',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
