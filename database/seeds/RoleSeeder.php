<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = Role::create(['name' => 'SuperAdmin']);
        $merchantAdmin = Role::create(['name' => 'Merchant Admin']);
        $merchantUser = Role::create(['name' => 'Merchant User']);

        /** permissions
         * 1. user.create
         * 2. user.read
         * 3. user.update
         * 4. user.delete
         * 5. attendance.create
         * 6. attendance.read
         * 7. attendance.update
         * 8. attendance.delete
         * 9. merchant.create
         * 10. merchant.read
         * 11. merchant.update
         * 12. merchant.delete
         */

        $superAdmin->permissions()->sync([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
        $merchantAdmin->permissions()->sync([1, 2, 3, 4, 5, 6, 7, 8, 10, 11]);
        $merchantUser->permissions()->sync([2, 6, 10]);
    }
}
