<?php

use App\Merchant;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $merchant = Merchant::insert([
            'name' => 'Butter QR',
            'token' => 'butterQrDev',
            'enabled' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
