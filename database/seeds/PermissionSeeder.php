<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert(['name' => 'user.create']);
        Permission::insert(['name' => 'user.read']);
        Permission::insert(['name' => 'user.update']);
        Permission::insert(['name' => 'user.delete']);

        Permission::insert(['name' => 'attendance.create']);
        Permission::insert(['name' => 'attendance.read']);
        Permission::insert(['name' => 'attendance.update']);
        Permission::insert(['name' => 'attendance.delete']);

        Permission::insert(['name' => 'merchant.create']);
        Permission::insert(['name' => 'merchant.read']);
        Permission::insert(['name' => 'merchant.update']);
        Permission::insert(['name' => 'merchant.delete']);
    }
}
