<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'username' => $this->username,
            'merchant' => new MerchantResourceMinimal($this->merchant),
            'role' => $this->role->name,
            'permissions' => $this->role->permissions->pluck('name')->toArray(),
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
        ];

        return $data;
    }
}
