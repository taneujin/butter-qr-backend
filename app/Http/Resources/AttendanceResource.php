<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttendanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'name' => $this->name,
            'identification_number' => $this->identification_number,
            'phone_number' => $this->phone_number,
            'temperature' => $this->temperature,
            'ip_address' => $this->ip_address ? $this->ip_address : null,
            'url' => $this->url ? $this->url : null,
            'device_type' => $this->device_type ? $this->device_type : null,
            'merchant' => new MerchantResourceMinimal($this->merchant),
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
        ];

        return $data;
    }
}
