<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MerchantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'name' => $this->name,
            'email' => $this->email,
            'address' => $this->address,
            'token' => $this->address,
            'extra' => $this->extra,
            'enabled' => !!$this->enabled,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
        ];

        return $data;
    }
}
