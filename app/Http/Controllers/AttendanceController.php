<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Exceptions\MerchantNotEnabledException;
use App\Exceptions\ModelNotFoundException;
use App\Http\Resources\AttendanceResource;
use App\Http\Resources\MerchantResourceMinimal;
use App\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AttendanceController extends Controller
{
    public function create(Request $request)
    {
        Validator::make($request->all(), [
            'merchant_token' => 'required|string',
            'name' => 'required|string|max:255',
            'identification_number' => 'required|string|min:12|max:12',
            'phone_number' => 'required|string',
            'temperature' => 'required|numeric'
        ])->validate();

        $merchant = Merchant::where('token', $request->merchant_token)->first();
        if (!$merchant) {
            throw new ModelNotFoundException();
        }

        // throw exception if the merchant is not enabled
        if (!$merchant->enabled) {
            throw new MerchantNotEnabledException();
        }

        // register new attendance
        $attendance = new Attendance();
        $attendance->merchant_id = $merchant->id;
        $attendance->name = $request->name;
        $attendance->identification_number = $request->identification_number;
        $attendance->phone_number = $request->phone_number;
        $attendance->temperature = $request->temperature;

        // optional details that may not be passed
        $attendance->ip_address = $request->ip_address ? $request->ip_address : null;
        $attendance->url = $request->url ? $request->url : null;
        $attendance->device_type = $request->device_type ? $request->device_type : null;
        $attendance->save();

        // inject merchant data into attendance
        $attendance->merchant = new MerchantResourceMinimal($merchant);

        return response()->json([
            'code' => 0,
            'message' => 'Data created successfully',
            'data' => new AttendanceResource($attendance),
        ]);
    }


    /**
     * Functions used by CMS
     */

    public function index(Request $request)
    {
        // user can only query based on which merchant they are registered to
        $merchantId = auth('api')->user()->merchant_id;

        $query = Attendance::where('merchant_id', $merchantId);

        // append additional filters if additional inputs are passed in
        if ($request->input('from_date')) {
            $query->where('created_at', '>', $request->input('from_date'));
        }

        if ($request->input('to_date')) {
            $query->where('created_at', '<', $request->input('to_date'));
        }

        $paginationLimit = $request->input('pagination_limit') ? $request->input('pagination_limit') : 25;
        $items = $query->with('merchant')->paginate($paginationLimit);
        $data = $items->getCollection();

        return response()->json([
            'code' => 0,
            'data' => AttendanceResource::collection($data),
            'pagination' => [
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'per_page' => $items->perPage(),
                'total' => $items->total(),
                'count' => $items->count(),
            ],
        ]);
    }

    public function show($id)
    {
        // user can only query based on which merchant they are registered to
        $merchantId = auth('api')->user()->merchant_id;

        $query = Attendance::where('id', $id)
            ->where('merchant_id', $merchantId)
            ->with('merchant')
            ->first();

        if (!$query) {
            throw new ModelNotFoundException();
        }

        return response()->json([
            'code' => 0,
            'data' => new AttendanceResource($query),
        ]);
    }
}
