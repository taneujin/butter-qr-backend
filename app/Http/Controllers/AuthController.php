<?php

namespace App\Http\Controllers;

use App\Exceptions\UnauthorizedException;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => [
            'logout',
            'me',
            'refresh',
        ]]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'password' => 'required|string|min:6',
        ])->validate();

        $credentials = request(['username', 'password']);

        if (!$token = auth('api')->attempt($credentials)) {
            throw new UnauthorizedException();
        }

        $user = auth('api')->user();

        return response()->json([
            'code' => 0,
            'data' => [
                'token' => $token,
                'user' => new UserResource($user),
            ],
        ]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $token = auth('api')->refresh();

        return response()->json([
            'code' => 0,
            'data' => [
                'token' => $token,
            ]
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json([
            'code' => 0,
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Change password of the user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });

        Validator::make($request->all(), [
            'old_password' => 'required|alpha_num|old_password:' . auth('api')->user()->password,
            'new_password' => 'required|alpha_num|min:6|confirmed|different:old_password',
        ])->validate();

        $user = auth('api')->user();
        $user->password = Hash::make($request->get('new_password'));
        $user->save();

        return response()->json([
            'code' => 0,
            'message' => 'Password changed successfully',
        ]);
    }
}
