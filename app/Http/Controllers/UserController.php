<?php

namespace App\Http\Controllers;

use App\Exceptions\ModelNotFoundException;
use App\Http\Resources\UserResource;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    /**
     * Functions used by CMS
     */

    public function index(Request $request)
    {
        // user can only query based on which merchant they are registered to
        $merchantId = auth('api')->user()->merchant_id;

        $query = User::where('merchant_id', $merchantId);

        // append additional filters if additional inputs are passed in
        if ($request->input('from_date')) {
            $query->where('created_at', '>', $request->input('from_date'));
        }

        if ($request->input('to_date')) {
            $query->where('created_at', '<', $request->input('to_date'));
        }

        $paginationLimit = $request->input('pagination_limit') ? $request->input('pagination_limit') : 25;
        $items = $query->with('merchant')->paginate($paginationLimit);
        $data = $items->getCollection();

        return response()->json([
            'code' => 0,
            'data' => UserResource::collection($data),
            'pagination' => [
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'per_page' => $items->perPage(),
                'total' => $items->total(),
                'count' => $items->count(),
            ],
        ]);
    }

    public function show($id)
    {
        // user can only query based on which merchant they are registered to
        $merchantId = auth('api')->user()->merchant_id;

        $query = User::where('id', $id)
            ->where('merchant_id', $merchantId)
            ->with('merchant')
            ->first();

        if (!$query) {
            throw new ModelNotFoundException();
        }

        return response()->json([
            'code' => 0,
            'data' => new UserResource($query),
        ]);
    }

    public function create(Request $request)
    {
        Validator::make($request->all(), [
            'username' => 'required|string|unique:users',
            'password' => 'required|string|max:255',
            'role' => 'required', 'string', Rule::in(['Merchant Admin', 'Merchant User'])
        ])->validate();

        $user = auth('api')->user();

        $newUser = new User();
        $newUser->username = $request->input('username');
        $newUser->password = Hash::make($request->input('password'));

        // perform role look up for its id
        $role = Role::where('name', $request->input('role'))->first();
        $newUser->role_id = $role->id;

        $newUser->merchant_id = $user->merchant_id;
        $newUser->created_by = $user->username;
        $newUser->save();

        // attach role to user
        $newUser->role = $role;

        return response()->json([
            'code' => 0,
            'message' => 'Data created successfully',
            'data' => new UserResource($newUser),
        ]);
    }

    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'id' => 'required|numeric|min:1',
            'role' => 'required', 'string', Rule::in(['Merchant Admin', 'Merchant User'])
        ])->validate();

        $user = auth('api')->user();
        $merchant = auth('api')->user()->merchant;

        $userToUpdate = User::where('id', $request->input('id'))->where('merchant_id', $merchant->id)->first();

        if (!$userToUpdate) {
            throw new ModelNotFoundException();
        }

        // perform role look up for its id
        $role = Role::where('name', $request->input('role'))->first();
        $userToUpdate->role_id = $role->id;
        $userToUpdate->save();

        // attach role and merchant to user
        $userToUpdate->role = $role;
        $userToUpdate->merchant = $merchant;

        return response()->json([
            'code' => 0,
            'message' => 'Data updated successfully',
            'data' => new UserResource($userToUpdate),
        ]);
    }
}
