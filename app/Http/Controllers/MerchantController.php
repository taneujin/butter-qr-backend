<?php

namespace App\Http\Controllers;

use App\Exceptions\ModelNotFoundException;
use App\Http\Resources\MerchantResource;
use App\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MerchantController extends Controller
{
    /**
     * Functions used by CMS
     */

    public function index(Request $request)
    {
        $query = Merchant::query();

        // append additional filters if additional inputs are passed in
        if ($request->input('from_date')) {
            $query->where('created_at', '>', $request->input('from_date'));
        }

        if ($request->input('to_date')) {
            $query->where('created_at', '<', $request->input('to_date'));
        }

        $paginationLimit = $request->input('pagination_limit') ? $request->input('pagination_limit') : 25;
        $items = $query->paginate($paginationLimit);
        $data = $items->getCollection();

        return response()->json([
            'code' => 0,
            'data' => MerchantResource::collection($data),
            'pagination' => [
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'per_page' => $items->perPage(),
                'total' => $items->total(),
                'count' => $items->count(),
            ],
        ]);
    }

    public function show($id)
    {
        $query = Merchant::where('id', $id)->first();

        if (!$query) {
            throw new ModelNotFoundException();
        }

        return response()->json([
            'code' => 0,
            'data' => new MerchantResource($query),
        ]);
    }

    public function create(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string|unique:merchants',
            'email' => 'sometimes|email',
            'address' => 'sometimes|string',
        ])->validate();

        $merchant = new Merchant();
        $merchant->name = $request->input('name');
        $merchant->email = $request->input('email') ? $request->input('email') : null;
        $merchant->address = $request->input('address') ? $request->input('address') : null;
        $merchant->enabled = true;
        $merchant->save();

        return response()->json([
            'code' => 0,
            'message' => 'Data created successfully',
            'data' => new MerchantResource($merchant),
        ]);
    }

    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'id' => 'required|numeric|min:1',
            'name' => 'required|string',
            'email' => 'sometimes|email',
            'address' => 'sometimes|string',
            'enabled' => 'required|boolean',
        ])->validate();

        $merchantToUpdate = Merchant::where('id', $request->input('id'))->first();

        if (!$merchantToUpdate) {
            throw new ModelNotFoundException();
        }

        // perform role look up for its id
        $merchantToUpdate->name = $request->input('name');
        $merchantToUpdate->email = $request->input('email');
        $merchantToUpdate->address = $request->input('address');
        $merchantToUpdate->enabled = !!$request->input('enabled');
        $merchantToUpdate->save();

        return response()->json([
            'code' => 0,
            'message' => 'Data updated successfully',
            'data' => new MerchantResource($merchantToUpdate),
        ]);
    }
}
