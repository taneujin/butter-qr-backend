<?php

namespace App\Http\Middleware;

use Closure;

class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions)
    {
        $permissions_array = explode('|', $permissions);
        $user_permissions = auth('api')->user()->role->permissions->pluck('name');

        foreach ($permissions_array as $permission) {
            if ($user_permissions->contains($permission)) {
                return $next($request);
            }
        }

        throw new UnauthorizedException(403);
    }
}
