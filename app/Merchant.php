<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;
use Illuminate\Support\Str;

class Merchant extends Model
{
    protected $cast = [
        'enabled' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->token = Str::random(16);
            $model->created_at = $model->freshTimestamp();
        });
    }
}
