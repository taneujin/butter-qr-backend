<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ValidationException) {
            return response()->json([
                'code' => 1000,
                'errors' => $exception->validator->messages(),
            ])->setStatusCode(400);
        }

        if ($exception instanceof ModelNotFoundException) {
            return response()->json([
                'code' => 1001,
                'message' => 'Resource not found',
            ])->setStatusCode(404);
        }

        if ($exception instanceof UnauthorizedException) {
            // Return default validation if all the exception above not match
            return response()->json([
                'code' => 1002,
                'message' => 'Unauthorized',
            ])->setStatusCode(401);
        }

        if ($exception instanceof MerchantNotEnabledException) {
            // Return default validation if all the exception above not match
            return response()->json([
                'code' => 1003,
                'message' => 'Merchant is disabled',
            ])->setStatusCode(401);
        }

        return parent::render($request, $exception);
    }
}
