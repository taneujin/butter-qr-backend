<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
        'name',
        'identification_number',
        'phone_number',
        'ip_address',
        'url',
        'device_type',
    ];

    public function merchant() {
        return $this->hasOne(Merchant::class, 'id');
    }
}
