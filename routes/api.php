<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'prefix' => 'cms'
], function () {

    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::post('/login', 'AuthController@login');
        Route::post('/refresh', 'AuthController@refresh');
        Route::post('/logout', 'AuthController@logout');
        Route::post('/change-password', 'AuthController@changePassword');
    });

    Route::group([
        'prefix' => 'merchant'
    ], function () {
        Route::get('/', [
            'uses' => 'MerchantController@index',
            'middleware' => 'has.permissions:merchant.read'
        ]);

        Route::get('/{id}', [
            'uses' => 'MerchantController@show',
            'middleware' => 'has.permissions:merchant.read'
        ]);

        Route::post('/create', [
            'uses' => 'MerchantController@create',
            'middleware' => 'has.permissions:merchant.create'
        ]);

        Route::post('/update', [
            'uses' => 'MerchantController@update',
            'middleware' => 'has.permissions:merchant.update'
        ]);
    });

    Route::group([
        'prefix' => 'user'
    ], function () {
        Route::get('/', [
            'uses' => 'UserController@index',
            'middleware' => 'has.permissions:user.read'
        ]);

        Route::get('/{id}', [
            'uses' => 'UserController@show',
            'middleware' => 'has.permissions:user.read'
        ]);

        Route::post('/create', [
            'uses' => 'UserController@create',
            'middleware' => 'has.permissions:user.create'
        ]);

        Route::post('/update', [
            'uses' => 'UserController@update',
            'middleware' => 'has.permissions:user.update'
        ]);
    });

    Route::group([
        'prefix' => 'attendance'
    ], function () {
        Route::get('/', [
            'uses' => 'AttendanceController@index',
            'middleware' => 'has.permissions:attendance.read'
        ]);

        Route::get('/{id}', [
            'uses' => 'AttendanceController@show',
            'middleware' => 'has.permissions:attendance.read'
        ]);
    });
});

Route::group([
    'prefix' => 'attendance'
], function () {
    Route::post('/create', [
        'uses' => 'AttendanceController@create',
    ]);
});
